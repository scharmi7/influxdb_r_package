# R package for InfluxDB

## Installation

The package can be directly installed from Bitbucket sources using the remotes library:

```R
library(remotes)

install_bitbucket("scharmi7/influxdb_r_package")
```


## Examples

```R
library(influxR)

# InfluxDB settings
db_host <- "nrchbs-sld4057"
db_name <- "d3"

# Select first 10 rows from series "measurement"
query <- "SELECT * FROM \"measurement\" LIMIT 10"

readFromInfluxDB(db_host, db_name, query)
```